import scrapy
import os
import re

from scrapy.spiders import CrawlSpider
from imgspider.items import ScrapyItem
from imgspider.pipelines import MongoPipeline



class QuotesSpider(CrawlSpider):
    name = "imgscrapy"

    def start_requests(self):
        scrapurl = os.environ.get("scrapurl")
        urls = []
        urls.append(scrapurl)

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)
        


    def parse(self, response):
        page = response.url.split("/")[-1]
        imgurl_main = response.url.split("/")[2]
        check_date = os.environ.get("check_date")
        img_main = response.css("img").xpath("@src").extract()

        #deal with img data, make url path to be correct
        img_main_new = []

        for i in img_main:
            while i.rsplit(".", 1)[1] in ["png", "jpg", "jpeg"]:

                if re.match(r'http(s?)://(.*)', i):
                    img_main_new.append(i)
                    break

                elif re.match(r'//.*', i):
                    i = i.replace("//", "")
                    i = "http://" + i
                    img_main_new.append(i)
                    break

                else:
                    i = ("http://" + imgurl_main + i)
                    img_main_new.append(i)
                    break

        item = ScrapyItem()

        item["imgurl"] = imgurl_main
        item["title"] = page
        item["img_src"] = img_main_new
        item["date"] = check_date

        yield item



