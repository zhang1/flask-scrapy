import os
import subprocess
import datetime
import re

from subprocess import PIPE
from flask_pymongo import PyMongo
from flask import Flask, render_template, request, jsonify



def create_app():
    app = Flask(__name__)

    #connect mongodb
    app.config["MONGO_URI"] = "mongodb://172.21.100.2:27017/mongodb"
    mongo = PyMongo(app)

    #home page
    @app.route('/')
    def index():
        return render_template('index.html')

    @app.route('/bs4')
    def bs4():
        url_bs4 = request.args.get('url', 0)

        # run subprocess
        os.putenv('bs4url', str(url_bs4))
        p_bs4 = subprocess.Popen(['python3', 'testbs4.py'], cwd="/app/app/app", stdout=PIPE)
        p_bs4.wait()

        # get and deal with data
        stdout_bs4 = p_bs4.communicate()
        print(stdout_bs4)
        chtuple_to_str = str(stdout_bs4).split("[", 1)[1].rsplit("]", 1)[0]

        bs4_data_str = re.sub("(\s)?'", "", chtuple_to_str)
        bs4_data_list = bs4_data_str.split(',')

        for i in bs4_data_list:
            print(i)

        return jsonify(bs4_data_list)
    
    #use scrapy to crawl
    #get url from browser,and set env, let scrapy can get url
    #after scrapy done, send data to browser
    @app.route('/scrapy')
    def scrapy():
        url = request.args.get('url', 0)
        now = datetime.datetime.now()
        check_date = str(now)

        os.putenv('scrapurl', str(url))
        os.putenv('check_date', check_date) 

        spider_name = "imgscrapy"
        p_scrapy = subprocess.check_call(['scrapy', 'crawl', spider_name,], cwd="/app/app/app")
        
        crawl_scrapy = mongo.db.mongodb.find_one({"date": check_date})

        return jsonify(crawl_scrapy["img_src"])

    @app.route('/curl')
    def curl():
        url = request.args.get('url', 0)
        domain_name_curl = url.split("/")[2]

        # run subprocess
        process_curl = subprocess.Popen(["curl", url], stdout=PIPE, shell=False)

        # get and sort data
        stdout = process_curl.communicate()
        del_space = re.sub("(\s)", "", str(stdout))
        method_img_tag = re.compile('imgsrc=".*?"')
        find_img_tag = method_img_tag.findall(del_space)
        
        ch_to_str = ''.join(find_img_tag)
        method_src_atr = re.compile(r'"(.*?)"')

        find_src_atr = method_src_atr.findall(ch_to_str)
        print(find_src_atr)

        # src is different,sort of data
        curl_list = []
        for i in find_src_atr:
            while i.rsplit(".", 1)[1] in ["png", "jpg", "jpeg"]:
                if re.match(r'http(s?)://(.*)', i):
                    curl_list.append(i)
                    break

                elif re.match(r'//.*', i):
                    i = i.replace("//", "")
                    i = "http://" + i
                    curl_list.append(i)
                    break
                    
                else:
                    i = "http://" + domain_name_curl + i
                    curl_list.append(i)
                    break
    
        return jsonify(curl_list)

    return app
 