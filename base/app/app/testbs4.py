import os 
import requests
import re

from urllib.request import urlopen
from bs4 import BeautifulSoup
# if has Chinese, apply decode()
url = os.getenv('bs4url')

domain_name_curl = url.split("/")[2]

html = requests.get(url)

soup = BeautifulSoup(html.text, "html.parser")

el = soup.find_all("img")

bs4_list = []

for i in el:
    while i["src"].rsplit(".", 1)[1] in ["png", "jpg", "jpeg"]:
        src = str(i["src"])
        if re.match(r'http(s?)://(.*)', src):
            bs4_list.append(src)
            break

        elif re.match(r'//.*', src):
            src = src.replace("//", "")
            src = "http://" + src
            bs4_list.append(src)
            break
            
        else:
            src = "http://" + domain_name_curl + src
            bs4_list.append(src)
            break

        # bs4_list.append(i["src"])
        # break

print(bs4_list)



