from scrapy.item import Item, Field


class ScrapyItem(Item):
    imgurl = Field()
    title = Field()
    img_src= Field()
    date = Field()