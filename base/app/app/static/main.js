// get page and change 
function changepage(id) {
    var changepage = document.getElementsByClassName("container");
    var showpage = document.getElementById(id);
    var i = 0;
    for (i = 0; i < changepage.length; i++) {
        changepage[i].style.display = "none";
    }
    showpage.style.display = "block";
}
document.getElementById("defaultOpen").click();

// get variable
var all_index = [index_bs4=0, index_scrapy=0, index_curl=0];
var all_id = ["url_bs4", "url_scrapy", "url_curl"]
var all_img = ["bs4_img", "scrapy_img", "curl_img"]
var all_link = ["/bs4", "/scrapy", "/curl"]
var bs4_data, scrapy_data, curl_data;
var all_data = [];
all_data.push(bs4_data, scrapy_data, curl_data);

// thumbnail image controls
function plusimg(index, n) {
    all_index[n] += index;
    showimg(n);
}

// show image when change next
function showimg(n) {
    if (all_data[n]) {
        var img = document.getElementById(all_img[n]);
        while (all_index[n] > (all_data[n].length - 1)) {
            all_index[n] = 0;
        }
        while (all_index[n] < 0) {
            all_index[n] = all_data[n].length -1;
        }
        img.src = all_data[n][all_index[n]];

    } else {
        alert("no image");
    }
}

// send data to server
function geturl(n) {
    var btn = document.getElementById(all_id[n]).nextElementSibling.textContent;
    var urlname = document.getElementById(all_id[n]).value;
    
    while ((btn === "submit") && urlname) {
        document.getElementById(all_id[n]).nextElementSibling.innerHTML = "wait";

        $.getJSON($SCRIPT_ROOT + all_link[n], {
                url :urlname
            }, function(data) {
                    all_data[n] = data;
                    var show_img = document.getElementById(all_img[n]);
                    show_img.src = all_data[n][0];          
            });
        
        document.getElementById(all_id[n]).nextElementSibling.innerHTML = "submit";
        break

    }
}

